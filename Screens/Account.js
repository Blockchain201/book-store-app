import { useEffect, useState } from "react";
import {
    Text,
    View,
    ActivityIndicator,
    StyleSheet,
    Image
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
function Account() {
    const [currentAccountPK, setCurrentAccountPK] = useState();
    const [createAccountStatus, setCreateAccountStatus] =
        useState(false);
    return (
        <View style={styles.rootContainer}>
            <View style={styles.bodyContainer}>
                <View style={styles.headerContainer}>
                    <Text style={styles.headerTitle}>My Book Store</Text>
                    <Text style={styles.headerTagLine}></Text>
                </View>
                <View style={styles.body}>
                    <View>
                        <Text style={styles.label}></Text>
                        <Text style={styles.value} selectable={true}></Text>
                    </View>
                    <View style={styles.prvateKeyContainer}>
                        <Text style={styles.label}>Welcome to the Book Store App</Text>
                    </View>
                    <View style={styles.imageContainer}>
                        <Image
                            source={require("../assets/logo.jpg")}
                            style={styles.image}
                        />
                    </View>

                    <View style={styles.prvateKeyContainer}>
                        <Text style={styles.label}>Explore More</Text>

                        {!createAccountStatus ? (
                            <Ionicons
                                style={{ paddingTop: 10 }}
                                name="add-circle-sharp"
                                size={42}
                                color="#007193"
                            />
                        ) : (
                            <ActivityIndicator size="large" color="white" />
                        )}
                    </View>
                    <View style={styles.prvateKeyContainer}>
                        <Text style={styles.label}></Text>
                        <Text style={{ paddingTop: 5 }}></Text>
                    </View>
                </View>
            </View>
        </View>
    );
}
export default Account;
const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
    },
    headerContainer: {
        alignItems: "center",
        backgroundColor: "#007193",
        paddingVertical: 20,
    },
    headerTitle: {
        fontSize: 30,
        fontWeight: "bold",
        color: "white",
        paddingVertical: 20,
    },
    headerTagLine: {
        color: "white",
        fontSize: 17,
    },
    imageContainer: {
        height: 150,
        width: 150,
        alignSelf: "center",
        borderRadius: 75,
        overflow: "hidden",
        marginBottom: 20,
    },
    image: {
        width: "100%",
        height: "100%",
    },
    body: {
        flex: 1,
        justifyContent: "space-between",
    },
    label: {
        fontSize: 20,
        fontWeight: "bold",
        paddingVertical: 10,
        paddingLeft: 20,
    },
    value: {
        fontSize: 15,
        fontWeight: "bold",
        paddingVertical: 5,
        paddingLeft: 20,
    },
    bodyContainer: {
        flex: 1,
        justifyContent: "space-between",
        backgroundColor: "#E2D7D7"
    },
    footer: {
        backgroundColor: "#31689f",
        paddingVertical: 25,
        justifyContent: "center",
        alignItems: "center",
    },
    footerBalanceLoadingText: {
        color: "white",
        fontSize: 16,
    },
    prvateKeyContainer: {
        alignItems: "center",
        paddingLeft: 5,
        paddingRight: 10,

    },
    btn: {
        alignSelf: "center", // Center horizontally
        paddingTop: 10,
        paddingLeft: 5,
        paddingRight: 10
    },
});