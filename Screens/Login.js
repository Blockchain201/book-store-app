import {
    View,
    Text,
    StyleSheet,
    Button,
    ActivityIndicator,
    TextInput,
} from "react-native";
import { useState } from "react";
export default function Login() {
    const [login, setLogin] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [createAccountState, setCreateAccountState] = useState(false);

    const guideLines = ["1. Click on generate passphrase",
        "2. Copy the passphrase that you generate. It can be used to login\
   later on or to recover your account",
        "3. Click on Create Account to create Account in our wallet",
        "4. In case if you want different passphrase click on Clear and repeat\ from step 1",
        "5. If you already have an account then use the login button ",
    ]
    return (
        <View style={styles.rootContainer}>
            {!createAccountState ? (
                <>
                    <View style={styles.header}>
                        <Text style={styles.title}>Welcome to Book Store</Text>
                        {!login ? (
                            <Text style={styles.tagLine}>
                                Follow the steps given below to create an account
                            </Text>
                        ) : null}
                    </View>
                    {!login ? (
                        <View style={styles.body}>
                            {guideLines.map((step) => (
                                <View key={step} style={styles.guideLinesContainer}>
                                    <Text style={styles.guideLinesText}>{step}</Text>
                                </View>
                            ))
                            }
                            <View style={styles.generatePassPhrase}>
                                <Text
                                    style={styles.buttonText}>generatePassPhrase</Text>
                                {isLoading ? (
                                    <ActivityIndicator size="small" color="white" />
                                ) : null}
                            </View>
                            <View style={styles.loginButtonLink}>
                                <Button title="Login" onPress={() =>
                                    setLogin(true)} />
                            </View>
                        </View>
                    ) : (

                        <View style={styles.loginContainer}>
                            <Text style={styles.loginLabel}>Enter Your
                                Passphrase:</Text>
                            <TextInput
                                placeholder="Enter your passphrase"
                                style={styles.loginBox}
                                multiline={true}
                            />
                            <View style={styles.loginButton}>
                                <Button title="Wallet Login" />
                            </View>
                        </View>
                    )}
                </>
            ) : (
                <ActivityIndicator size="large" color="white" />
            )}
        </View>
    );
}
const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: "#a4c5e6",
    },
    header: {
        alignItems: "center",
        backgroundColor: "#0a4f95",
        paddingVertical: 20,
    },
    title: {
        fontSize: 30,
        fontWeight: "bold",
        color: "white",
        paddingVertical: 20,
    },
    tagLine: {
        color: "white",
        fontSize: 17,
    },
    buttonsAction: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 10,
    },
    generatePassPhrase: {
        marginTop: 5,
        marginBottom: 10,
        alignSelf: "center",
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: "#2196F3",
        borderRadius: 100,
    },
    buttonText: {
        color: "white",
        fontSize: 16,
        textAlign: "center",
    },
    guideLinesContainer: {
        margin: 7,
        padding: 7,
        // paddingVertical: 15,
    },
    guideLinesText: {
        color: "#000000",
        fontSize: 16,
        fontWeight: "bold",
    },
    passPhraseContainer: {
        // backgroundColor: "#ffffff",
        elevation: 5,
        paddingVertical: 10,
        paddingHorizontal: 5,
        marginBottom: 10,
    },
    loginContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    loginButtonLink: {
        marginHorizontal: 20,
        elevation: 5,
    },
    loginBox: {
        backgroundColor: "white",
        alignSelf: "stretch",
        paddingVertical: 10,
        paddingHorizontal: 5,
        marginHorizontal: 20,
    },
    loginButton: {
        marginTop: 5,
        paddingVertical: 5,
        paddingHorizontal: 10,
    },
    loginLabel: {
        fontSize: 18,
        fontWeight: "bold",
        paddingBottom: 5,
    },
});