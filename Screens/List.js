import React, { useState } from 'react';
import {
    View,
    Text,
    FlatList,
    Modal,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';

const bookData = [
    {
        id: '1',
        title: 'Merchant of Venice',
        author: 'William Shakespeare',
        price: '$10.99',
    },
    {
        id: '2',
        title: 'Dawa',
        author: 'Kinzang Choden',
        price: '$12.99',
    },
    {
        id: '3',
        title: 'The Monk Who Sold His Ferrari',
        author: 'Robin Sharma',
        price: '$12.99',
    },
    {
        id: '4',
        title: 'The Leader Who Had No Title',
        author: 'Robin Sharma',
        price: '$12.99',
    },
    {
        id: '5',
        title: 'Notebook',
        author: 'Nicholas Spark',
        price: '$12.99',
    },
    {
        id: '6',
        title: 'The Choice',
        author: 'Nicholas Spark',
        price: '$12.99',
    },
];

function List() {
    const [modalVisible, setModalVisible] = useState(false);
    const [selectedBookId, setSelectedBookId] = useState(null);

    const openModal = (bookId) => {
        setSelectedBookId(bookId);
        setModalVisible(true);
    };

    const closeModal = () => {
        setModalVisible(false);
    };

    const selectedBook = bookData.find((book) => book.id === selectedBookId);

    return (
        <View style={styles.container}>
            <View style={styles.headerContainer}>
                <Text style={styles.headerTitle}>List of Books</Text>
            </View>
            <FlatList
                style={styles.d}
                data={bookData}
                keyExtractor={(item) => item.id}
                renderItem={({ item }) => (
                    <TouchableOpacity onPress={() => openModal(item.id)}>
                        <View style={styles.bookItem}>
                            <Text style={styles.title}>{item.title}</Text>
                        </View>
                    </TouchableOpacity>
                )}
            />

            <Modal visible={modalVisible} animationType="slide">
                <View style={styles.modalContainer}>
                    {selectedBook && (
                        <View style={styles.bookDetails}>
                            <Text style={styles.title}>{selectedBook.title}</Text>
                            <Text style={styles.author}>{selectedBook.author}</Text>
                            <Text style={styles.price}>{selectedBook.price}</Text>
                        </View>
                    )}
                    <TouchableOpacity onPress={closeModal}>
                        <Text style={styles.closeButton}>Close</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    bookItem: {
        padding: 16,
        borderBottomWidth: 1,
        borderColor: '#ccc',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    author: {
        fontSize: 16,
    },
    price: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#E2D7D7"
    },
    headerContainer: {
        alignItems: "center",
        backgroundColor: "#007193",
        paddingVertical: 20,
    },
    headerTitle: {
        fontSize: 30,
        fontWeight: "bold",
        color: "white",
        paddingVertical: 20,
    },
    d: {
        backgroundColor: "#E2D7D7"
    },
    bookDetails: {
        padding: 16,
    },
    closeButton: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'blue',
    },
});

export default List;