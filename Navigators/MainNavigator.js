import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Login from "../Screens/Login";
import SecondaryNavigator from "./SecondaryNavigator";

const Stack = createNativeStackNavigator();

function MainNavigator() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
            }}>
            <Stack.Screen name="SecondaryNavigator" component={SecondaryNavigator} />
            <Stack.Screen name="Login" component={Login} />
        </Stack.Navigator>

    )
}
export default MainNavigator;
