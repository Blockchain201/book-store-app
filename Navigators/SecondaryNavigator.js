// import { createBottomTabNavigator } from "@react-navigation/bottomtabs";
import { Ionicons, FontAwesome5, FontAwesome } from "@expo/vector-icons";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Account from "../Screens/Account";
import List from "../Screens/List";
const Tab = createBottomTabNavigator();
function SecondaryNavigator() {
    return (
        <Tab.Navigator
            screenOptions={{
                headerShown: false,
                tabBarShowLabel: false,
            }}
            sceneContainerStyle={{ backgroundColor: "#a4c5e6" }}
        >
            <Tab.Screen
                name="Account"
                component={Account}
                options={{
                    tabBarIcon: ({ color, size }) => (
                        <Ionicons name="wallet-sharp" size={size} color={color} />
                    ),
                }}
            />
            <Tab.Screen
                name="List"
                component={List}
                options={{
                    tabBarIcon: ({ color, size }) => (
                        <FontAwesome name="list-ul" color={color} size={size} />
                    ),
                }}
            />
            {/* <Tab.Screen
                name="Transaction"
                component={Transaction}
                options={{
                    tabBarIcon: ({ color, size }) => (
                        <FontAwesome5 name="history" size={size} color={color} />
                    ),
                }}
            /> */}
        </Tab.Navigator>
    );
}
export default SecondaryNavigator;